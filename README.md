# magpiebridge-simple

This is a small walk-through that provides a (KISS -- keep it short and simple)
example to setup an initial project for
[MagpieBridge](https://github.com/MagpieBridge/MagpieBridge). The goal is to
guide users towards building a first working version (i.e., a minimal viable
prototype MVP) of a language server and client that displays results from a
simple JSON file. 

This walk-through is based on the great tutorials in the [MagpieBridge WIKI](https://github.com/MagpieBridge/MagpieBridge/wiki). The focus, however,
is a little different. With this walk-through we are trying to get up to speed
quickly by providing a working MVP quickly on which you can build upon.

Below is a very simplistic illustration of the magpiebridge-simple simple.
Let's assume we have a JSON file that contains findings that have been produced
by a static analysis tool. Our tiny LSP server implementation parsers
information from the JSON file and displays it in VSCode by means of an LSP
client plugin.

``` mermaid
graph LR
    A[Finding JSON file] -->|parse| B(KISS MagpieBridge Server)
    B -->|LSP| C(VSCode LSP Client)
    C -->|LSP| B
```

Let's further assume that an entry for a finding in the JSON
report provides the most basic information: `message` provides a description
text for the finding, `location.file` provides the information regarding the
file to which the finding refers, and `line` refers to the line in the source
code. In this particular example, we assume that the findings in the JSON file
where produced by a static analysis tool for Go.

``` json
{
  "vulnerabilities": [
    {
      "message": "This line could potentially leak a secret 'username'.",
      "location": {
        "file": "main.go",
        "line": 12
      }
    },
    {
      "message": "This line could potentially leak a secret 'password'.",
      "location": {
        "file": "main.go",
        "line": 13
      }
    }
  ]
}
```

The Go code [main.go](demo-go/main.go) to which the findings above refer is
displayed below. You can see that sensitive information seems to be stored in
the variables `username` and `password` in line 12 and 13, respectively. Our
goal is to get the results from the JSON file above, displayed in when opening
[main.go](demo-go/main.go) with VSCode.

``` golang
package main

import (
    "fmt"
)

func main() {
    fmt.Println("main")
}

func secret() {
    username := "admin"
    var password = "f62e5bcda4fae4f82370da0c6f20697b8f8447ef"
    fmt.Println("Doing something with: ", username, password)
}
```

The project in this repository is written in Kotlin with Gradle as a build
system. Apart from the Kotlin sources, the directory structure of this
repository contains three notable elements:
1. `results.json`: This is the file that contains the findings.
1. demo-go: This is the Go demo project which contains the file above. Our goal
   is to open this project and get the results `results.json` displayed in
   VSCode.
1. vscode: This is the actual VSCode LSP client that connects with the
   MagpieBridge-based LSP server to display the results in the IDE.


First, we introduce the server implementation before we look into the client
implementation. The last part illustrates how you can build and run the tool.

### Server 

The core building blocks are similar for all JVM based languages. The
dependenencies for our small MagpieBridge-based language server can be found in
the
[build.gradle.kts](https://gitlab.com/julianthome/magpiebridge-simple/-/blob/main/build.gradle.kts#L15).
Most importantly we require the latest MagpieBridge version
`com.github.magpiebridge:magpiebridge:0.2.0-SNAPSHOT`.

The remainder of the `build.gradle.kts` relates mostly to building a
self-contained, executable `.jar` file.

The implementation contains four classes/files: 
1. [main.kt](src/main/kotlin/com/gitlab/magpiebridge/main.kt) the entry point
   where we configure and start the language server.
1. [Findings.kt](src/main/kotlin/com/gitlab/magpiebridge/Finding.kt) a simple
   class that represent a finding.
1. [Utils.kt](src/main/kotlin/com/gitlab/magpiebridge/Utils.kt) a helper
   class that provides two functions: 
   - [extractResults](https://gitlab.com/julianthome/magpiebridge-simple/-/blob/main/src/main/kotlin/com/gitlab/magpiebridge/Utils.kt#L16) 
   parses the `report.json` file, generates `Findings` objects (based on
   `Findings.kt` and stores them as results.
   - [columnsFromLine](https://gitlab.com/julianthome/magpiebridge-simple/-/blob/main/src/main/kotlin/com/gitlab/magpiebridge/Utils.kt#L50)
   is a utility function that computes the widths of the lines so that we can
   properly define the boundaries of the source code segments that should be
   highlihged in VSCode. This is useful if your analysis tool only provides
   line (and no column) information.
1. [Analysis.kt](src/main/kotlin/com/gitlab/magpiebridge/Analysis.kt) is the
   core (analysis) implementation of our LSP server. The [analyze](https://gitlab.com/julianthome/magpiebridge-simple/-/blob/main/src/main/kotlin/com/gitlab/magpiebridge/Analysis.kt#L28) method is a 
   callback is invoked to (re)run the analysis. In our case, the
   analysis just [parses the `report.json` file](https://gitlab.com/julianthome/magpiebridge-simple/-/blob/main/src/main/kotlin/com/gitlab/magpiebridge/Analysis.kt#L23), 
   translates the JSON vulnerabilities into `Finding` objects which are then
   translated into [AnalysisResult](https://gitlab.com/julianthome/magpiebridge-simple/-/blob/main/src/main/kotlin/com/gitlab/magpiebridge/Analysis.kt#L43) objects. 
    

In `main.kt` we are [launching the language server](https://gitlab.com/julianthome/magpiebridge-simple/-/blob/main/src/main/kotlin/com/gitlab/magpiebridge/main.kt#L26)
on port 5007 providing `report.json` as parameter [using the default MagpieBridge configuration](https://gitlab.com/julianthome/magpiebridge-simple/-/blob/main/src/main/kotlin/com/gitlab/magpiebridge/main.kt#L30).
Then, we are [hooking our custom Analysis](https://gitlab.com/julianthome/magpiebridge-simple/-/blob/main/src/main/kotlin/com/gitlab/magpiebridge/main.kt#L34)
for Go into MagpieBridge. Our custom analysis does not do much apart from
reading in the `report.json` file, and passing the results to the LSP client
(i.e., our VSCode plugin).

### Client

The VSCode plugin implementation is built on top of the [vscode-languageclient](https://gitlab.com/julianthome/magpiebridge-simple/-/blob/main/vscode/package.json#L44)
Node.js module. It [connects to the language server on port 5007](https://gitlab.com/julianthome/magpiebridge-simple/-/blob/main/vscode/src/extension.ts#L10). In 
the `LanguageClientOptions` dictionary we define the file extensions to trigger
a (re)analysis.

### Installation & Execution

In order to run the whole project you can invoke the `./runall.sh` script which
will build the project, install the VSCode plugin, and run VSCode on the
test-project. Note that VSCode has to be installed for the script to work.

``` bash
#!/bin/bash

# build the language server
gradle clean build || {
    echo "unable to build kotlin project"
    exit 1
}

# build vscode plugin
(cd vscode; npm install; node_modules/vsce/vsce package)
# run LSP server
java -jar build/libs/magpiebridge-gitlab-1.0-SNAPSHOT.jar -r "./report.json" &
# start IDE with extension on our test project
(cd demo-go && code --install-extension ../vscode/magpiebridge-simple-0.0.1.vsix && code .)

exit 0
```

The screencast below illustrates what you should see when executing the script.

![Running magpiebridge-simple](/img/screencast.gif)

