#!/bin/bash

# build the language server
gradle clean build || {
    echo "unable to build kotlin project"
    exit 1
}

# build vscode plugin
(cd vscode; npm install; node_modules/vsce/vsce package)
# run LSP server
java -jar build/libs/magpiebridge-gitlab-1.0-SNAPSHOT.jar -r "./report.json" &
# start IDE with extension on our test project
(cd demo-go && code --install-extension ../vscode/magpiebridge-simple-0.0.1.vsix && code .)

exit 0
