package com.gitlab.magpiebridge

class Finding(
    var fileName: String,
    var msg: String,
    var startLine: Int,
    var endLine: Int
){
    override fun toString(): String {
        val builder = StringBuilder()
        builder.append("filename:")
        builder.append(fileName)
        builder.append('\n')
        builder.append("msg:")
        builder.append(msg)
        builder.append('\n')
        builder.append("startLine:")
        builder.append(startLine)
        builder.append('\n')
        builder.append("endLine:")
        builder.append(endLine)
        builder.append('\n')
        return builder.toString()
    }
}