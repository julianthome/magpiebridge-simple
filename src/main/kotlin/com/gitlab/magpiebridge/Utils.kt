package com.gitlab.magpiebridge

import com.google.gson.Gson
import com.google.gson.JsonIOException
import com.google.gson.JsonObject
import com.google.gson.JsonSyntaxException
import org.apache.commons.lang3.tuple.MutablePair
import java.io.*
import java.net.URL
import java.util.logging.Logger

class Utils {
    companion object {
        private val LOG = Logger.getLogger(Utils::class.simpleName)

        fun extractResults(resultsPath: String): Set<Finding> {
            LOG.info("parsing json reports in $resultsPath")
            val result: MutableSet<Finding> = HashSet()

            if (!File(resultsPath).exists()) {
                LOG.info("$resultsPath not present")
                return result
            }

            try {
                val jsonString = File(resultsPath).readText(Charsets.UTF_8)
                LOG.info(jsonString)
                val obj = Gson().fromJson(jsonString, JsonObject::class.java)
                for (vuln in obj.getAsJsonArray("vulnerabilities")) {
                    val jsonObj = vuln.asJsonObject
                    val message = jsonObj.get("message").asString
                    val location = jsonObj.getAsJsonObject("location").asJsonObject
                    val startLine: Int = location.get("line").asInt
                    val endLine: Int = location.get("line").asInt
                    val file = location.get("file").asString
                    val finding = Finding(file, message, startLine, endLine)
                    result.add(finding)
                }
            } catch (e: JsonIOException) {
                throw RuntimeException(e)
            } catch (e: JsonSyntaxException) {
                throw RuntimeException(e)
            } catch (e: FileNotFoundException) {
                throw RuntimeException(e)
            }

            return result
        }

        fun columnsFromLine(infile: URL, startLine: Int, endLine: Int): MutablePair<Int, Int> {
            val columnPair: MutablePair<Int, Int> = MutablePair(1, 1)
            val file = File(infile.file)
            try {
                var lines = BufferedReader(FileReader(file)).useLines { it.toList() }
                var i = 1
                for (line in lines) {
                    if (i != startLine && i != endLine) {
                        i++
                        continue
                    }

                    var nline = line.split("//").get(0)
                    if (i == startLine) {
                        var cidx = 0
                        while (cidx < nline.toCharArray().size &&
                            (nline.toCharArray()[cidx] == ' ' || nline.toCharArray()[cidx] == '\t')
                        ) {
                            cidx += 1
                        }

                        columnPair.left = Math.max(cidx, 1)
                    }
                    if (i == endLine) {
                        var cidx = Math.max(nline.toCharArray().size - 1, 0)
                        while (cidx > 0 && nline.toCharArray()[cidx] == ' ') {
                            cidx -= 1
                        }
                        columnPair.right = cidx + 1
                    }
                    i++
                }
            } catch (e: IOException) {
                e.stackTrace
            }
            return columnPair
        }
    }
}