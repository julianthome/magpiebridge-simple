package com.gitlab.magpiebridge

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import kotlin.io.path.pathString
import kotlin.io.path.writeText

internal class UtilsKtTest {

    @Test
    fun testUtils() {
        var code = """
func foo1() {
	db, err := sql.Open("sqlite3", ":memory:")
	if err != nil {
		panic(err)
	}
	rows, err := db.Query("SELECT * FROM foo WHERE name = " + os.Args[1])
	if err != nil {
		panic(err)
	}
	//test
	defer rows.Close()
}
    """.trimIndent()

        val tmpfile = kotlin.io.path.createTempFile("tmp")
        tmpfile.writeText(code)
        tmpfile.toUri()
        try {
            val ret = Utils.columnsFromLine(
                tmpfile.toUri().toURL(),
                2,
                2
            )
            assertEquals(ret.left, 1)
            assertEquals(ret.right, 43)
        } catch (e: Exception) {
            e.stackTrace
            println(e.message)
        }
    }
}

