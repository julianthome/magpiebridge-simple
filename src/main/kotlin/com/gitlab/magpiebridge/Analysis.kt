package com.gitlab.magpiebridge

import com.ibm.wala.cast.tree.CAstSourcePositionMap
import com.ibm.wala.classLoader.IMethod
import com.ibm.wala.classLoader.Module
import com.ibm.wala.classLoader.SourceFileModule
import com.ibm.wala.util.collections.Pair
import magpiebridge.core.*
import magpiebridge.util.SourceCodeReader
import org.eclipse.lsp4j.DiagnosticSeverity
import java.io.IOException
import java.io.Reader
import java.net.MalformedURLException
import java.net.URL
import java.util.logging.Logger

class Analysis(val reportFile: String) : ServerAnalysis {
    private val LOG = Logger.getLogger(Analysis::class.simpleName)
    private val results: MutableSet<Finding> = HashSet()

    override fun source() = "Simple MagpieBridge example"

    private fun readResults(resultsFile: String) {
        LOG.info("reread results")
        results.addAll(Utils.extractResults(resultsFile))
    }

    override fun analyze(files: Collection<Module>, server: AnalysisConsumer, rerun: Boolean) {
        LOG.info("rerun analysis")
        this.results.clear()
        try {
            if (rerun) {
                readResults(reportFile)
                val results: Set<AnalysisResult> = runAnalysisOnFiles(files, server as MagpieServer)
                server.consume(results, source())
            }
        } catch (e: MalformedURLException) {
            throw RuntimeException(e)
        }
    }

    @Throws(MalformedURLException::class)
    fun runAnalysisOnFiles(files: Collection<Module>, server: MagpieServer): Set<AnalysisResult> {
        val results: MutableSet<AnalysisResult> = HashSet()
        for (file in files) {
            if (file is SourceFileModule) {
                for (res in this.results) {
                  val clientURL = URL(server.getClientUri(file.url.toString()))
                  val pos: CAstSourcePositionMap.Position = object : CAstSourcePositionMap.Position {
                      override fun getFirstCol() =
                          Utils.columnsFromLine(this.url, this.firstLine, this.lastLine)
                              .left

                      override fun getLastCol() =
                          Utils.columnsFromLine(this.url, this.firstLine, this.lastLine)
                              .right

                      @Throws(IOException::class)
                      override fun getReader(): Reader? {
                          return null
                      }

                      override fun compareTo(other: IMethod.SourcePosition) = 0
                      override fun getFirstLine() = res.startLine
                      override fun getLastLine() = res.endLine
                      override fun getFirstOffset() = 0
                      override fun getLastOffset() = 0
                      override fun getURL() = clientURL
                  }
                  val r: AnalysisResult = convert(res, pos)
                  results.add(r)
                }
            }
        }
        return results
    }

    private fun convert(result: Finding, pos: CAstSourcePositionMap.Position): AnalysisResult {
        return object : AnalysisResult {
            override fun toString(useMarkdown: Boolean) = result.msg
            override fun severity(): DiagnosticSeverity = DiagnosticSeverity.Error
            override fun repair() = Pair.make(pos, "")
            override fun related(): Iterable<Pair<CAstSourcePositionMap.Position, String>> {
                return ArrayList()
            }
            override fun position() = pos
            override fun kind() = Kind.Diagnostic
            override fun code(): String {
                var code: String? = null
                try {
                    code = SourceCodeReader.getLinesInString(pos)
                } catch (e: Exception) {
                }
                if (code != null) {
                    return code
                }
                return ""
            }
        }
    }
}
