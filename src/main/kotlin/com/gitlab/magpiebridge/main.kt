package com.gitlab.magpiebridge

import magpiebridge.core.*
import magpiebridge.core.MagpieServer
import magpiebridge.core.ServerAnalysis
import magpiebridge.core.ServerConfiguration
import magpiebridge.core.ToolAnalysis
import org.eclipse.lsp4j.jsonrpc.messages.Either
import java.util.logging.Logger
import kotlinx.cli.*
import java.io.File

private val LOG = Logger.getLogger("main")

fun main(args: Array<String>) {
    val parser = ArgParser("magpiebridge-simple")
    val report by parser.option(ArgType.String, shortName = "r", description = "report directory").required()
    parser.parse(args)

    if (!File(report).exists() && !File(report).isFile) {
        println("Directory $report does not exist")
        kotlin.system.exitProcess(-1)
    }

    LOG.info("report file is $report")

    MagpieServer.launchOnSocketPort(5007) { createServer(report) }
}

private fun createServer(reportFile: String): MagpieServer? {
    // set up configuration for MagpieServer
    class LanguageHandler : LanguageExtensionHandler {
        override fun getLanguageForExtension(extension: String?): String {
            if (extension == "go") {
                return "go"
            }
            return ""
        }

        override fun getExtensionsForLanguage(language: String?): MutableSet<String> {
            if (language == "go")
                return mutableSetOf(".go")
            return mutableSetOf()
        }
    }

    class serverConfig : ServerConfiguration() {
        override fun getLanguageExtensionHandler(): LanguageExtensionHandler {
            return LanguageHandler()
        }
    }

    val server = MagpieServer(serverConfig())
    val myAnalysis: ServerAnalysis = Analysis(reportFile)
    val analysis = Either.forLeft<ServerAnalysis, ToolAnalysis>(myAnalysis)
    server.addAnalysis(analysis, "go")
    return server
}
